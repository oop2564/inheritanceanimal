/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.inheritance;

/**
 *
 * @author Administrator
 */
public class Duck extends Animal {
    private int numOfWings;
    public Duck(String name, String color) {
        super(name, color, 2);
        System.out.println("Duck created");
    }
    
    public void fly() {
        System.out.println("Duck: " + this.name + " can fly !!!");
    }
    
    @Override
    public void walk() {
        System.out.println("Duck: " + this.name + " walk with " 
                + this.numOfLegs + " legs.");
    }
    
    @Override
    public void speak() {
        System.out.println("Duck: " + this.name + " speak > Quack Quack!!!");
    }
}
