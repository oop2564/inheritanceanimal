/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.inheritance;

/**
 *
 * @author Administrator
 */
public class Animal {

    protected String name;
    public int numOfLegs = 0;
    private String color;

    public Animal(String name, String color, int numOfLegs) {
        System.out.println("Animal created");
        this.name = name;
        this.color = color;
        this.numOfLegs = numOfLegs;
    }

    public void walk() {
        System.out.println("Animal walk");
    }

    public void speak() {
        System.out.println("Animal speak");
        System.out.println("Name: " + this.name + " Color: " + this.color
                + " Number Of Legs: " + this.numOfLegs);
    }
    
    public void BreakRange() {
        System.out.println("-------------------------");
    }
}
