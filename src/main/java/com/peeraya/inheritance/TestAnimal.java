/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.inheritance;

/**
 *
 * @author Administrator
 */
public class TestAnimal {
    public static void main(String[] args) {
        Animal animal = new Animal("Ani", "White", 0);
        animal.speak();
        animal.walk();
        animal.BreakRange();
        
        Dog dang = new Dog("Dang", "Black&White");
        dang.speak();
        dang.walk();
        dang.BreakRange();
        
        Dog to = new Dog("To", "Brown");
        to.speak();
        to.walk();
        to.BreakRange();
        
        Dog bat = new Dog("Bat", "White&Black");
        bat.speak();
        bat.walk();
        bat.BreakRange();
        
        Dog mome = new Dog("Mome", "White&Black");
        mome.speak();
        mome.walk();
        mome.BreakRange();

        Cat zero = new Cat("Zero", "Orange");
        zero.speak();
        zero.walk();
        zero.BreakRange();
        
        Duck zom = new Duck("Zom", "Yellow");
        zom.speak();
        zom.walk();
        zom.fly();
        zom.BreakRange();
        
        Duck gabgab = new Duck("GabGab", "Yellow");
        gabgab.speak();
        gabgab.walk();
        gabgab.fly();
        gabgab.BreakRange();
        
        System.out.println("Animal is Animal: " + (animal instanceof Animal));
        System.out.println("Animal is Dog: " + (animal instanceof Dog));
        System.out.println("Animal is Cat: " + (animal instanceof Cat));
        System.out.println("Animal is Duck: " + (animal instanceof Duck));
                
        System.out.println("Dang is Animal: " + (dang instanceof Animal));
        System.out.println("Dang is Dog: " + (dang instanceof Dog));
        System.out.println("To is Animal: " + (to instanceof Animal));
        System.out.println("To is Dog: " + (to instanceof Dog));
        System.out.println("Bat is Animal: " + (bat instanceof Animal));
        System.out.println("Bat is Dog: " + (bat instanceof Dog));
        System.out.println("Mome is Animal: " + (mome instanceof Animal));
        System.out.println("Mome is Dog: " + (mome instanceof Dog));
        
        System.out.println("Zero is Animal: " + (zero instanceof Animal));
        System.out.println("Zero is Cat: " + (zero instanceof Cat));
        
        System.out.println("Zom is Animal: " + (zom instanceof Animal));
        System.out.println("Zom is Duck: " + (zom instanceof Duck));
        System.out.println("GabGab is Animal: " + (gabgab instanceof Animal));
        System.out.println("GabGab is Duck: " + (gabgab instanceof Duck));
        animal.BreakRange();
        
        Animal[] animals = {dang, to, bat, mome, zero, zom, gabgab};
        for (int i=0; i<animals.length; i++) {
            animals[i].walk();
            animals[i].speak();
            if(animals[i] instanceof Duck) {
                Duck duck = (Duck)animals[i];
                duck.fly();
            }
            System.out.println("");
        }
    }
}
