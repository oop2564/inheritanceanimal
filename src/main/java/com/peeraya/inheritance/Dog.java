/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.inheritance;

/**
 *
 * @author Administrator
 */
public class Dog extends Animal {
    public Dog(String name, String color) {
        super(name, color, 4);
        System.out.println("Dog created");
    }    
    
    @Override
    public void walk() {
        super.walk();
        System.out.println("Dog: " + this.name + " walk with " 
                + this.numOfLegs + " legs.");
    }
    
    @Override
    public void speak() {
        super.speak();
        System.out.println("Dog: " + this.name + " speak > Box Box!!!");
    }
}
